import React, { Component } from 'react';
import './App.css';

class App extends Component {

	constructor(props){
		super(props);
		this.state = {
			response: '',
			users : []
		}
		this.renderPerolas = this.renderPerolas.bind(this);
	}

	componentDidMount(){
		this.callApi()
			.then(res => {
				console.log(res.express)
				this.setState({ response: res.express })
			})
			.catch(err => console.log(err));

		this.callTeste()
			.then(res => {
				console.log(res.users);
				this.setState({ users: res.users })
			})
			.catch(err => console.log(err));
	}

	callApi = async () => {
		const response = await fetch('/api/mensagem');
		const body = await response.json();
		if ( response.status !== 200) throw Error(body.message);

		return body;
	};

	callTeste = async () => {
		const response = await fetch('/api/users');
		const body = await response.json();
		if ( response.status !== 200) throw Error(body.message);

		return body;
	};

	renderPerolas(perola){
		return(
			<h1 key={perola.id}>{perola.phrase}</h1>
		);
	};

	render() {

		return (
			<div className="App">
				
				<p className="App-intro">{this.state.response}</p>

				<ul>
				{this.state.users.map((user,i) => <li key={i}>{user.username}</li>)}
				</ul>

			</div>
		);
	}
}

export default App;