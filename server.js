const express = require('express');
const request = require('request');

const app = express();
const port = process.env.PORT || 5000;

app.get('/api/mensagem', (req, res) => {
	res.send({ express: 'Hello From Express' });
});

app.get('/api/users', (req, res) => {
	
	request('https://my-json-server.typicode.com/xandynnn/redbook/users', function (xreq, xres) {
		res.send({ users: JSON.parse(xres.body) });
	});

});

app.listen(port, () => console.log(`Listening on port ${port}`));